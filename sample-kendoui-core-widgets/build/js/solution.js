(function (global) {
    var solutionViewModel,
        app = global.app = global.app || {};
    
     function shuffle(array) {
      var currentIndex = array.length, temporaryValue, randomIndex ;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }
    
    solutionViewModel = kendo.data.ObservableObject.extend({
    	onViewInit: function () {console.log ('init')},
		onViewShow: function () {
            var that = this;
                
           $('#list').kendoTouch({
                            tap: function (e) {
                                var initialObj = e.touch.initialTouch;
                                
                                if (initialObj.nodeName === "LI") {
                                    alert (initialObj.innerHTML);
                                } 
                            }
                        }); 

        },
        author: "N",
        title:null,
        audio:null,
        image: null,
        pins:null,
        solution: {
            correctAnswer:3,
            incorrectAnswers: [5, 7, 9]
        },
        availableAnswers: function() {
            var that = this,
                myArray = that.solution.incorrectAnswers;
            
            myArray.push(that.solution.correctAnswer);
            
            myArray = shuffle(myArray);
            return myArray;
        }
        
         
    })
    
    app.solution = {
        viewModel: new solutionViewModel()
    };
})(window);
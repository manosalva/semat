(function (global) {
    var libraryViewModel,
        app = global.app = global.app || {};
    
    function loadLibraryFile() {
        var libraryData = app.fileApp._readTextFromFile('library2');
        return libraryData;
    }
    
    function createLibraryFile() {//create a new file if not found
        var libraryData=JSON.stringify([{"text":"hi"}]);
        app.fileApp._writeTextToFile('library2', libraryData);
    }
    
    libraryViewModel = kendo.data.ObservableObject.extend({
    	onViewInit: function () {
           var libraryData = loadLibraryFile();
            if (libraryData == undefined) {
                var create = createLibraryFile();
                alert ('created' + create);
            } else {
                alert ('library data' + libraryData);
                
            }
        },
		onViewShow: function () {},
        books:['sample book 1', 'sample book 2']
    })
    
    app.library = {
        viewModel: new libraryViewModel()
    };
})(window);
(function (global) {
    var provideSolutionViewModel,
        app = global.app = global.app || {};
    
    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }


    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    function shuffle(array) {
      var currentIndex = array.length, temporaryValue, randomIndex ;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }
    
    provideSolutionViewModel = kendo.data.ObservableObject.extend({
        onViewInit: function() {},
        onViewShow: function() {app.provideSolution.viewModel.solution.init()},
        solution: {
            init: function() {
                var that = this;
                $("#numericEntry").kendoTouch({
                 tap: function (e) {
                        var initialObj = e.touch.initialTouch;
                        
                        if (initialObj.nodeName === "LI") {
                            that.enter (initialObj);
                        }
                    }
                });
            },
            polarity: function() {
                var num = app.provideSolution.viewModel.correctAnswer;
                if (num.charAt(0) === "-") {
                    num = num.replace('-','');
                } else {
                    num = "-" + num;
                }
                
                app.provideSolution.viewModel.set('correctAnswer',  num);
            },
            clear: function() {
                app.provideSolution.viewModel.set('correctAnswer',  '0');
            },
            enter: function(obj) {
                var fn = obj.getAttribute('data-fn'),
                    that = this;
                if (fn === 'enter') {
                  var prev = app.provideSolution.viewModel.correctAnswer,
                    newNum;
                    if ((obj.innerHTML === '.' && prev.indexOf('.') === -1) || obj.innerHTML !== '.') {
                        newNum = prev + obj.innerHTML;    
                        app.provideSolution.viewModel.set('correctAnswer',  newNum);
                    }
                } else {
                    that[fn]();
                }
            },
            createRandomIncorrectChoices: function() {
                app.provideSolution.viewModel.set('viewMode_setIncorrectAnswers', true);
                app.provideSolution.viewModel.set('viewMode_setCorrectAnswer', false);
                
                var total = 32,
                total_negInt = 0, total_posInt = 0, total_negFloat = 0, total_posFloat = 0,
                base = app.provideSolution.viewModel.correctAnswer,
                array = [];
                
                if (base.charAt(0) === "-" && base.indexOf('.') === -1) {//negative integer
                    total_negInt = total_posInt = .5;
                }
                if (base.charAt(0) === "-" && base.indexOf('.') !== -1) {//negative float
                    total_negInt = total_posInt = total_negFloat = total_posFloat = .25;
                }
                if (base.charAt(0) !== "-" && base.indexOf('.') !== -1) {//positive float
                    total_posInt = total_posFloat = 0.5;
                }
                if (base.charAt(0) !== "-" && base.indexOf('.') === -1) {//positive integer
                    total_posInt = 1;
                }
                console.log (total_negInt, total_posInt, total_negFloat, total_posFloat);
                
                for (var i = 0; i < total * total_posInt; i++) {
                    array.push(getRandomInt(0, base*1.5));
                }
                for (var i = 0; i < total * total_negInt; i++) {
                    array.push(-1*getRandomInt(0, base*1.5));
                }
                for (var i = 0; i < total * total_posFloat; i++) {
                    array.push(getRandomArbitrary(0, base*1.5).toFixed(2));
                }
                for (var i = 0; i < total * total_negFloat; i++) {
                    array.push(-1*getRandomArbitrary(0, base*1.5).toFixed(2));
                }
                var shuffledArray = shuffle(array);
                app.provideSolution.viewModel.set('incorrectAnswers_random',  shuffledArray);
                
                var incorrectList = $('#incorrectAnswers_random');
                incorrectList.kendoTouch({
                    tap: function(e) {
                        var initialObj = e.touch.initialTouch,
                            vm = app.provideSolution.viewModel,
                            theClass = "k-state-selected";
                        
                        if (initialObj.nodeName === "LI") {
                            var $node = $(initialObj);
                            if (vm.incorrectAnswers_selected.length < 3) {
                               $node.toggleClass(theClass);
                            } else {
                               if ($node.hasClass(theClass)) {
                                   $node.removeClass(theClass);
                               }
                            }
                            var selection = [];
                            incorrectList.find('.'+theClass).each(function(i,v) {
                                selection.push(v.innerHTML);
                            });
                            vm.set('incorrectAnswers_selected', selection);
                        }
                        
                    }
                })
            }
        },
        correctAnswer:'0',
        incorrectAnswers_selected:[],
        incorrectAnswers_random:[]
    });
    
    
     app.provideSolution = {
        viewModel: new provideSolutionViewModel()
    };
})(window);
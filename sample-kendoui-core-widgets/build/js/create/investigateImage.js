(function (global) {
    var investigateImageViewModel,
        app = global.app = global.app || {};
    
    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        
       /* var html='';
        for (var key in evt) {
            html += key + '  ' + evt[key] + '<br />';
        }*/
        $('#output').html ( evt.x.client );
        
        return {
          x: evt.x.client - rect.left,
          y: evt.y.client - rect.top
        };
        /*x: evt.clientX - rect.left,
          y: evt.clientY - rect.top*/
    }
    
    investigateImageViewModel = kendo.data.ObservableObject.extend({
        onViewInit: function () {
            
        },
		onViewShow: function () {
            $('#croppedImage').html('<img src="/build/images/minion.jpg" />');
            var that = this;
            app.investigateImage.viewModel.shapeToolbar.init();
        },
        shapeToolbar: {
            init: function() {
                $("#toolbar-shapes ul").kendoMobileButtonGroup({
                    select: function(e) {
                        console.log (this.current().data('icon'));
                        app.investigateImage.viewModel.set("shapeToolbar.selectedItem", this.current().data('icon'))
                    }
                });
                
                var pinWrap = document.getElementById('canvas-wrap'),
                    overlay = document.getElementById('touchOverlay');
                pinWrap.setAttribute('style','width:' + app.settings.canvasDimensions().x + 'px; height:' + app.settings.canvasDimensions().y + 'px');
                $(overlay).kendoTouch({
                    tap: function (e) {
                        var initialObj = e.touch.initialTouch;
                        
                        if (initialObj.nodeName === "I") {
                            initialObj.remove();
                        } else {                               
                            var pos = getMousePos(overlay, e.touch),
                                pin = document.createElement('i');
                            pin.setAttribute('style', 'left:'+pos.x+'px; top:'+pos.y+'px;');
                            pin.setAttribute('data-icon', app.investigateImage.viewModel.shapeToolbar.selectedItem);
                            overlay.appendChild(pin);
                         }
                    }
                });
            },
            selectedItem: null
        },
     pins: []
    });
    
    
     app.investigateImage = {
        viewModel: new investigateImageViewModel()
    };
})(window);
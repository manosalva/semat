(function (global) {
    var askQuestionViewModel,
        app = global.app = global.app || {};
    
    askQuestionViewModel = kendo.data.ObservableObject.extend({
        soundRecorder: {
             record:function() {
                console.log ('record');
                var that = this;
                app.storyRecorder._capureAudio.apply(that, arguments)  
            },
            play:function() {
                console.log ('play');
                var that = this;
               app.storyRecorder._play.apply(that, arguments)
            },
            pause: function() {
               var that = this;
               app.storyRecorder._pause.apply(that, arguments)
            },
            stop: function() {
                var that = this;
               app.storyRecorder._stop.apply(that, arguments)
            }
        }
    });
    
    
     app.askQuestion = {
        viewModel: new askQuestionViewModel()
    };
})(window);
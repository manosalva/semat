(function (global) {
    //jan@borginteractive.com @ semat
    var app = global.app = global.app || {};
	    document.addEventListener("deviceready", function () {
           navigator.splashscreen.hide();
           app.storyRecorder = new StoryRecorder();
           app.fileApp = new FileApp();
           app.fileApp.run();
           app.system = new kendo.mobile.Application( $(document.body), {
               initial: "#view_library"
           });
        }, false);
     document.addEventListener("touchstart", function() {}, false);
    
    app.settings = {};
    app.settings.canvasDimensions = function() {
        var $window = $(window),
            width,
            height;
        
        if ( $window.width() > $window.height()) {
            width = $window.width(),
			height = $window.height();
        } else {
            height = $window.width(),
			width = $window.height();
        }

        return {
           x: Math.round(width*0.7),
           y: Math.round(height*0.7)
        };   
     }
    
})(window);
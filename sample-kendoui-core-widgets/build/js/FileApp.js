function FileApp() {
}

FileApp.prototype = {
	fileSystemHelper: null,
	fileNameField: null,
	textField: null,
     
	run: function() {
		/*var that = this,
    		writeFileButton = document.getElementById("writeFileButton"),
    		readFileButton = document.getElementById("readFileButton"),
    		deleteFileButton = document.getElementById("deleteFileButton");
        
		that.fileNameField = document.getElementById("fileNameInput");
		that.textField = document.getElementById("textInput");
        
		writeFileButton.addEventListener("click",
										 function() { 
											 that._writeTextToFile.call(that); 
										 });
        
		readFileButton.addEventListener("click",
										function() {
											that._readTextFromFile.call(that);
										});
        
		deleteFileButton.addEventListener("click",
										  function() {
											  that._deleteFile.call(that)
										  });*/
        console.log ('file app running');
		fileSystemHelper = new FileSystemHelper();
	},
    saveImg: function(uri, fileName, folderName) {
    
        fileSystemHelper.exec(uri, fileName, folderName);
        
    },
    
	_deleteFile: function () {
		var that = this,
		    fileName = that.fileNameField.value;
        
		if (that._isValidFileName(fileName)) {
			fileSystemHelper.deleteFile(fileName, that._onSuccess, that._onError);
		}
		else {
			var error = { code: 44, message: "Invalid filename"};
			that._onError(error);
		}
	},
    
	_readTextFromFile: function(fileName) {
		var that = this;
		if (that._isValidFileName(fileName)) {
			fileSystemHelper.readTextFromFile(fileName, that._onSuccess, that._onError);
		}
		else {
			var error = { code: 44, message: "Invalid filename"};
			that._onError(error);
		}
	},
    
	_writeTextToFile: function(fileName, text) {
		var that = this;
    		/*fileName = that.fileNameField.value,
    		text = that.textField.value;*/

		if (that._isValidFileName(fileName)) {
			fileSystemHelper.writeLine(fileName, text, that._onSuccess, that._onError)
		}
		else {
			var error = { code: 44, message: "Invalid filename"};
			that._onError(error);
		}
	},
    
	_onSuccess: function(value) {
		/*var notificationBox = document.getElementById("result");
		notificationBox.textContent = value;*/
        return (value);
	},
    
	_onError: function(error) {
        alert (error.message);

	/*	var errorCodeDiv = document.createElement("div"),
    		errorMessageDiv = document.createElement("div"),
    		notificationBox = document.getElementById("result");

		errorCodeDiv.textContent = "Error code: " + error.code;
		errorMessageDiv.textContent = "Message: " + error.message;
        
		notificationBox.innerHTML = "";
		notificationBox.appendChild(errorCodeDiv);
		notificationBox.appendChild(errorMessageDiv);*/
	},
    
	_isValidFileName: function(fileName) {
		//var patternFileName = /^[\w]+\.[\w]{1,5}$/;

		return fileName.length > 2;
	}
}
function StoryRecorder() {
}

StoryRecorder.prototype = {
	isPlaying : false,
    mediaContent: null,
   
	_onMediaSuccess: function() {
		var that =  app.storyRecorder;
        that._showMessage("mediaSuccess");
	},
    
	_onError: function(error) {
		var errorMessage;
        var that =  app.storyRecorder;
        
		if(typeof error === "string") {
			errorMessage = error;
		} else {
			errorMessage = "code: " + error.code + "\n" +
				"message: " + error.message + "\n";
		}
        
		that._showMessage(errorMessage);
		that.isPlaying = false;
	},
    
    _onMediaStatusChanged: function(status) {
		console.log("Media status: " + status);
    },
    _play: function() {
        var that =  app.storyRecorder;
        if(that.isPlaying === false) {
             
            var src = app.newStory.viewModel.audio;
            src = 'file://' + src.split('file:/')[1];
            //src = "http://audio.ibeat.org/content/p1rj1s/p1rj1s_-_rockGuitar.mp3";
            console.log ('playing ' + src);
            that.mediaContent = new Media(src,
                                        function() {
										  that._onMediaSuccess.apply(that, arguments);
									  },
									  function() {
										  that._onError.apply(that, arguments);
									  },
                                      function() {
                                          that._onMediaStatusChanged.apply(that, arguments);
                                      }
            );

            that.mediaContent.play();
		    that._showMessage('Playing... ' + src);
		    that.isPlaying = true;
        }
	},
    
	_pause: function () {
		if(this.isPlaying === true) {
            this.mediaContent.pause();
    		this._showMessage('Paused');
            this.isPlaying = false;
        }
	},
    
	_stop: function () {
        this.mediaContent.stop();
		this._showMessage('');
		this.isPlaying = false;
	},
    
	_showMessage: function(text) {
        alert (text);
		var statusBox = document.getElementById('playing');
		statusBox.textContent = text;
	},
    
     _capureAudio:function() {
        var that = this;
        navigator.device.capture.captureAudio(function() {
             app.storyRecorder._captureSuccess.apply(that, arguments);
        }, function() { 
            app.storyRecorder._captureError.apply(that, arguments);
        }, {limit:1});
    },
    
    _captureSuccess:function(capturedFiles) {
        var i;
        for (i=0;i < capturedFiles.length;i+=1) {
           // media.innerHTML+='<p>Capture taken! Its path is: ' + capturedFiles[i].fullPath + '</p>'
        }
         app.storyRecorder._showMessage('saving... ' + capturedFiles[0].fullPath);
         app.newStory.viewModel.set("audio", capturedFiles[0].fullPath);
        //$('#sampleImg').attr('src', capturedFiles[0].fullPath);
    },
    
    _captureError:function(error) {
        if (window.navigator.simulator === true) {
            alert(error);
        }
        else {
            var that =  app.storyRecorder;
            that._showMessage( "An error occured! Code:" + error.code );
        }
    },
}
(function (global) {
    var boardViewModel,
        app = global.app = global.app || {};
    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
    }
    function draw(e){
        var pos = getMousePos(canvas, e);
        posx = pos.x;
        posy = pos.y;

        context.fillStyle = "#000000";
        context.fillRect (posx, posy, 4, 4);
    }

    boardViewModel = kendo.data.ObservableObject.extend({
        title: '',
        canvas: {
            width: app.settings.canvasDimensions().x,
            height: app.settings.canvasDimensions().y,
            imageSrc:  app.newStory.viewModel.image.cropped,
            _init:function() {
                
                 /*var canvas = document.getElementById('primaryBoard');
                canvas.width  = app.settings.canvasDimensions().x;
                canvas.height = app.settings.canvasDimensions().y; 
               
               var img = new Image(),
                   imgPath = app.newStory.viewModel.image.original,
                   ctx = canvas.getContext('2d');
                
                if (imgPath == '') imgPath = "assets/base.png";
         
                img.onload = function(){
                    ctx.drawImage(img,0,0);
                };
                img.src = imgPath;
                
                var pinWrap = document.getElementById('canvas-wrap');
                pinWrap.setAttribute('style','width:' + app.settings.canvasDimensions().x + 'px; height:' + app.settings.canvasDimensions().y + 'px');
                canvas.addEventListener("click", function(e) {
                    var pos = getMousePos(canvas, e),
                        pin = document.createElement('i');
                    pin.setAttribute('style', 'left:'+pos.x+'px; top:'+pos.y+'px;');
                    pinWrap.appendChild(pin);
                    pin.setAttribute('data-icon', app.board.viewModel.shapeToolbar.selectedItem);
                    pin.addEventListener("click", function(e) {
                        $(pin).remove();
                    });
                    
                });*/
            },
            pins: [
                {x:10,y:30,ico:'a',id:0}
            ]
        },
        
		onViewInit: function () {
		},
		onViewShow: function () {
			/*var that = this;
			
			 $('#originalImage > img').bind('load', function() {
				
			});
			setTimeout(function() {$('#originalImage').imagefit();}, 1000)
			*/
			//that._repositionBaseImage();
            
          /*  $('#originalImage > img').each(function(i, item) {
                var img_height = $(item).height();
                var div_height = $(item).parent().height();
                if(img_height<div_height){
                    //IMAGE IS SHORTER THAN CONTAINER HEIGHT - CENTER IT VERTICALLY
                    var newMargin = (div_height-img_height)/2+'px';
                    $(item).css({'margin-top': newMargin });
                }else if(img_height>div_height){
                    //IMAGE IS GREATER THAN CONTAINER HEIGHT - REDUCE HEIGHT TO CONTAINER MAX - SET WIDTH TO AUTO  
                    $(item).css({'width': 'auto', 'height': '100%'});
                    //CENTER IT HORIZONTALLY
                    var img_width = $(item).width();
                    var div_width = $(item).parent().width();
                    var newMargin = (div_width-img_width)/2+'px';
                    $(item).css({'margin-left': newMargin});
                }
            });*/
           
            app.board.viewModel.canvas._init();
            app.board.viewModel.shapeToolbar.init();
		},
        shapeToolbar: {
            init: function() {
                $("#toolbar-shapes ul").kendoMobileButtonGroup({
                    select: function(e) {
                        app.board.viewModel.set("shapeToolbar.selectedItem", this.current().data('icon'))
                    }
                });
            },
            selectedItem: null
        }
	});

     app.board = {
        viewModel: new boardViewModel()
    };
})(window);
(function (global) {
    var model,
        app = global.app = global.app || {};

    model = kendo.observable({
		onViewInit: function () {
		},
		onViewShow: function () {
			app.captureApp.run();
		}
	});

     app.sampleCapture = {
        viewModel: new model()
    };
})(window);